<?php
require_once('../conn.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = $_POST['id'] ?? null;

    // update to database table
    $statement = $conn->prepare("DELETE FROM products WHERE id = :id");

    $statement->bindValue(':id', $id);

    $statement->execute();
    header('Location: index.php');
}
