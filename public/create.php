<?php
require_once('../conn.php');
require_once('../assets/function/randomString.php');

$errors = [];

// define empty data
$title = '';
$price = '';
$description = '';
$product = [
    'image' => ''
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once('../assets/function/products/validate.php');

    // if no error
    if (empty($errors)) {

        // insert to database table
        $statement = $conn->prepare("INSERT INTO products (title, image, description, price, create_date) VALUES (:title, :image, :description, :price, :date)");

        $statement->bindValue(':title', $title);
        $statement->bindValue(':image', $imagePath);
        $statement->bindValue(':description', $description);
        $statement->bindValue(':price', $price);
        $statement->bindValue(':date', date('Y-m-d H:i:s'));

        $statement->execute();

        $title = '';
        $price = '';
        $description = '';
    }
}
require_once('../assets/views/partials/header.php')
?>
<div class="wrap">

    <h1>Product CRUD</h1>
    <p>
        <a href="index.php" type="button" class="btn btn-sm btn-secondary ">Back</a>
    </p>

    <?php require_once('../assets/views/products/form.php') ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>