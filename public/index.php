<?php
require_once('../assets/function/products/getAllProduct.php');
require_once('../assets/views/partials/header.php');

?>

<h1>Product CRUD</h1>
<p>
    <a href="create.php" type="button" class="btn btn-sm btn-success">Create Product</a>
</p>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Image</th>
            <th scope="col">Title</th>
            <th scope="col">Price</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($products as $i => $product) : ?>
            <tr>
                <th scope="row"><?php echo $i + 1 ?></th>
                <td>
                    <img style="width: 50px;" src="<?php echo $product['image'] ?>" alt="">
                </td>
                <td><?php echo $product['title'] ?></td>
                <td>&#36;<?php echo $product['price'] ?></td>
                <td>
                    <a href="update.php?id=<?php echo $product['id'] ?>" type="button" class="btn btn-sm btn-outline-primary">Edit</a>
                    <form style="display: inline-block;" action="delete.php" method="POST">
                        <input type="hidden" name="id" value="<?php echo $product['id'] ?>">
                        <button href="#" type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>