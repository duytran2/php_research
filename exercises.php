<?php
// Edit your input
$num = 5;
$inpEx4 = 'zero;three;five;six;eight;one';
$valueEx1 = $_GET['inputEx1'] ?? null;
$valueEx4 = $_GET['inputEx4'] ?? null;
$valueEx5 = $_GET['inputEx5'] ?? null;

// exercise 1
function tableOfNumber($n)
{
    echo '<thead> <tr> <th scope="col">Table of ' . $n . '</th> </tr>
    <tbody></thead>';
    for ($i = 1; $i <= 10; $i++) {
        $result = $n * $i;
        echo '<tr> 
        <td>' . "$n * $i = $result" . '</td>
        </tr>';
    }
    echo '</tbody>';
}

// exercise 2
function triangleNormal($n)
{
    $result = '';
    for ($i = 0; $i < $n; $i++) {
        for ($j = 0; $j < $i + 1; $j++) {
            $result .= '*';
            echo '*';
        }
        $result .= "\n";
        echo '<br>';
    }
    return $result;
}

function triangleIsosceles($n)
{
    $result = '';

    // for from 1 to fix bug first line empty
    for ($i = 1; $i <= $n; $i++) {
        for ($space = 1; $space <= $n - $i; $space++) {
            $result .= ' ';
            echo '&nbsp;';
        }
        for ($point = 1; $point <= ($i * 2) - 1; $point++) {
            $result .= '*';
            echo '*';
        }
        echo '<br>';
        $result .= "\n";
    }
    return $result;
}

// exercise 3
function fizzBuzz()
{
    for ($i = 1; $i <= 100; $i++) {
        echo '<div class="fizz__buzz-item">';
        if (($i % 3 == 0) && ($i % 5 != 0)) {
            echo '<p class="fizz__buzz-text">Fizz</p>';
        } else if (($i % 5 == 0) && ($i % 3 != 0)) {
            echo '<p class="fizz__buzz-text">Buzz</p>';
        } else if (($i % 5 == 0) && ($i % 3 == 0)) {
            echo '<p class="fizz__buzz-text">FizzBuzz</p>';
        } else {
            echo '<p class="fizz__buzz-text">' . $i . '</p>';
        }
        echo '</div>';
    }
}

// exercise 4
function tranformText($rawText)
{
    $arrText = explode(';', $rawText);
    $result = '';
    foreach ($arrText as $text) {
        switch ($text) {
            case 'zero':
                $result .= '0';
                break;
            case 'one':
                $result .= '1';
                break;
            case 'two':
                $result .= '2';
                break;
            case 'three':
                $result .= '3';
                break;
            case 'four':
                $result .= '4';
                break;
            case 'five':
                $result .= '5';
                break;
            case 'six':
                $result .= '6';
                break;
            case 'seven':
                $result .= '7';
                break;
            case 'eight':
                $result .= '8';
                break;
            case 'nine':
                $result .= '9';
                break;

            default:
                'NaN';
                break;
        }
    }

    return $result;
}

// exercise 5
function uniqueList($numberText)
{
    // $arrText = explode(';', $numberText);
    $arr = array_map('intval', explode(',', $numberText));
    // $arr =  $number;
    sort($arr, SORT_DESC);

    $arr = array_unique($arr);

    $result = implode(',', $arr);

    return $result;
}

// print("Exercise 1 \n");
// printf(tableOfNumber($num) . "\n");

// print("Exercise 2 \n");
// printf("Isosceles Triangle \n" . triangleIsosceles($num) . "\n");
// printf("Normal Triangle \n" . triangleNormal($num) . "\n");

// print("Exercise 3 \n");
// printf(fizzBuzz() . "\n\n");

// print("Exercise 4 \n");
// print(tranformText($inpEx4) . "\n\n");

// print("Exercise 5 \n");
// print(uniqueList(6, 6, 8, 1, 1, 2, 2, 3, 4, 5, 5) . "\n\n");

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="./assets/css/app.css">
</head>

<body>

    <div class="form__wrap ">
        <form method="GET" action="">
            <div class="row g-3 align-items-center input-form">
                <div class="col-auto">
                    <label for="inputEx1" class="col-form-label">Exercise 1</label>
                </div>
                <div class="col-auto">
                    <input placeholder="Enter number" name="inputEx1" type="number" id="inputEx1" class="form-control" value="<?php echo $valueEx1; ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-secondary" id="btn-hide">Hide/Show</button>
        </form>
    </div>
    <?php if ($_SERVER['REQUEST_METHOD'] === 'GET') : if (isset($_GET['inputEx1']) && is_numeric($_GET['inputEx1'])) : ?>
            <table class="text-center table table-striped table-bordered table__number animate__animated  animate__fadeIn">
                <?php tableOfNumber($_GET['inputEx1']); ?>
            </table>
    <?php endif;
    endif; ?>

    <div class="triangle__wrap">
        <p class="ex-heading">Exercise 2</p>
        <?php triangleNormal(5); ?>
        <br>
        <?php triangleIsosceles(5); ?>
    </div>

    <div class="fizz__buzz-wrap">
        <p class="ex-heading">Exercise 3</p>
        <div class="fizz__buzz-item__wrap">
            <?php fizzBuzz(); ?>
        </div>
    </div>

    <div class="form__wrap">
        <form method="GET" action="">
            <div class="row g-3 align-items-center input-form">
                <div class="col-auto">
                    <label for="inputEx4" class="col-form-label">Exercise 4</label>
                </div>
                <div class="col-auto">
                    <input placeholder="Enter text" name="inputEx4" type="text" id="inputEx4" class="form-control" value="<?php echo $valueEx4; ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <?php if ($_SERVER['REQUEST_METHOD'] === 'GET') : if (isset($_GET['inputEx4']) && ($_GET['inputEx4'])) : ?>
            <p class="ex-result"><?php echo tranformText($_GET['inputEx4']) ?></p>
    <?php endif;
    endif; ?>

    <div class="form__wrap">
        <form method="GET" action="">
            <div class="row g-3 align-items-center input-form">
                <div class="col-auto">
                    <label for="inputEx5" class="col-form-label">Exercise 5</label>
                </div>
                <div class="col-auto">
                    <input placeholder="Enter text" name="inputEx5" type="text" id="inputEx5" class="form-control" value="<?php echo $valueEx5; ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <?php if ($_SERVER['REQUEST_METHOD'] === 'GET') : if (isset($_GET['inputEx5']) && ($_GET['inputEx5'])) : ?>
            <p class="ex-result"><?php echo uniqueList($_GET['inputEx5']) ?></p>

    <?php endif;
    endif; ?>


</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $('#btn-hide').on('click', function() {
        $('.table__number').toggle();
    })
</script>

</html>