<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach ($errors as $error) : ?>
            <div><?php echo $error; ?></div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<?php if ($product['image']) : ?>
    <img class="update-img" src="<?php echo $product['image'] ?>" alt="">
<?php endif; ?>

<form method="POST" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label class="form-label">Image</label>
        <input name="image" type="file" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Title</label>
        <input name="title" type="text" class="form-control" value="<?php echo $title ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Price</label>
        <input name="price" type="number" step=".01" class="form-control" value="<?php echo $price ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Description</label>
        <textarea name="description" type="text" class="form-control""><?php echo $description ?></textarea>
</div>
<button type=" submit" class="btn btn-primary">Submit</button>
</form>