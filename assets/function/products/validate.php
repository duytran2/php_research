<?php

// store POST data
$title = $_POST['title'];
$description = $_POST['description'];
$price = $_POST['price'];

// form validate
if (!$title) {
    $errors[] = 'Title is required';
}
if (!$price) {
    $errors[] = 'Price is required';
}


// create folder
if (!is_dir('upload/images')) {
    mkdir('upload/images', 0777, true);
}

// if no error
if (empty($errors)) {
    $image = $_FILES['image'] ?? null;
    $imagePath = '';

    // create path + upload picture
    if ($image && $image['tmp_name']) {
        if ($product['image']) {
            unlink($product['image']);
        }

        $imagePath = 'upload/images/' . randomString(8) . '/' . $image['name'];
        mkdir(dirname($imagePath));

        move_uploaded_file($image['tmp_name'], $imagePath);
    }
}
