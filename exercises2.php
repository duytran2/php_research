<?php
// Exercise 1

// Using strtotime
$valueCurrEx1 = $_GET['currentDate'] ?? null;
$valuePastEx1 = $_GET['pastDate'] ?? null;
$fromDate = $_GET['fromDate'] ?? null;
$numberDay = $_GET['numberDay'] ?? null;
function countDayStrtotime($currentDateString, $pastDateString)
{
    $diff = strtotime($currentDateString) - strtotime($pastDateString);

    // 1 day = 24 hours
    // 24 * 60 * 60 = 86400 seconds
    return abs(round($diff / 86400));
}

// Exercise 2
function addDay($currentDateString, $numberDay)
{
    $count = strtotime($currentDateString . '+' . $numberDay . 'days');

    $result = date('Y-m-d',  $count);

    return $result;
}
function minusDay($currentDateString, $numberDay)
{
    $count = strtotime($currentDateString . '-' . $numberDay . 'days');
    $result = date('Y-m-d',  $count);

    return $result;
}

// Exercise 3
function countDaysOfMonth()
{
    $currentMonth = intval(date('n'));
    $currentYear = intval(date('Y'));

    $result = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);

    echo  '<span class="ex-result">' . $result . ' </span><span>days in ' . $currentMonth . '/' . $currentYear . '</span>';
}

// Exercise 4
function jsonToArray()
{
    $arr = json_decode(' {"Title": "The Cuckoos Calling", "Author": "Robert Galbraith", "Detail": { "Publisher": "Little Brown" }} ', true);

    foreach ($arr as $key => $value) {
        if (!is_array($value)) {
            echo ($key . ' : ' . $value . '<br>');
        } else {
            foreach ($value as $subKey => $subValue) {
                echo ($subKey . ' : ' . $subValue . "\n");
            }
        }
    }
}

// Exercise 5
function sortArray($arr)
{
    asort($arr);
    echo ("ASC by value <br>");
    print_r($arr);
    echo '<br>';

    arsort($arr);
    echo ("DESC by value <br>");
    print_r($arr);
    echo '<br>';

    ksort($arr);
    echo ("ASC by key <br>");
    print_r($arr);
    echo '<br>';

    krsort($arr);
    echo ("DESC by key <br>");
    print_r($arr);
}

// Exercise 6
function temperature(...$numbers)
{
    $arr = array(...$numbers);
    $length = count($arr);

    $sum = 0;
    foreach ($arr as $value) {
        $sum += $value;
    }

    $avg = round($sum / $length, 1);
    echo ('Avg temp is:  <span class="ex-result">' . $avg . '</span> <br>');

    sort($arr);

    $arr = array_values(array_unique($arr));
    $length = count($arr);

    echo ("5 min temperature: ");
    for ($i = 0; $i < 5; $i++) {
        echo ("$arr[$i] ");
    }
    echo '<br>';

    echo ("5 max temperature: ");
    for ($i = $length - 5; $i < $length; $i++) {
        echo ("$arr[$i] ");
    }
    echo '<br>';
}

// Exercise 7
function countValueInArr($arr, $findValue)
{
    print_r($arr);
    $arrCount = array_count_values($arr);
    echo ('<br>' . $findValue . ' appeared <span class="ex-result">' . $arrCount[$findValue] . '</span> times');
}

// Exercise 8
function readFileCSV()
{
    if (($handle = fopen("data.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, ',')) !== FALSE) {
            $dateTime = date_create($data[3]);
            echo '  <tr>
                        <th scope="row">' . $data[0] . '</th>
                        <td>' . $data[1] . '</td>
                        <td>' . $data[2] . '</td>
                        <td>' . date_format($dateTime, 'Y/m/d H:i:s') . '</td>
                        <td>' . $data[4] . '</td>
                    </tr>';
        }
        fclose($handle);
    }
}

// Ex 1
// print(countDayStrtotime("2022-04-22", "2022-03-31") . "\n");

// // Ex 2
// print(addDay("13-03-2022", 40) . "\n");
// print(minusDay("22-04-2022", 40) . "\n");

// // Ex 3
// print(countDaysOfMonth() . "\n");

// // Ex 4
// jsonToArray();

// // Ex 5
// sortArray(["a" => 1, "b" => 3, "c" => 4, "d" => 5]);

// // Ex 6
// temperature(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73);

// // EX 7
// countValueInArr($arrEx7, 73);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 2</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="./assets/css/app.css">
</head>

<body>
    <!-- Exercise 1 -->
    <p class="ex-heading">Exercise 1</p>

    <div class="form__wrap">
        <form method="GET" action="">
            <div class="row g-3 align-items-center input-form">
                <div class="col-auto">
                    <label for="currentDate" class="col-form-label">From</label>
                </div>
                <div class="col-auto">
                    <input placeholder="Current date" name="currentDate" type="date" id="" class="form-control" value="<?php echo $valueCurrEx1; ?>">
                </div>
                <div class="col-auto">
                    <label for="pastDate" class="col-form-label">To</label>
                </div>
                <div class="col-auto">
                    <input placeholder="Past date" name="pastDate" type="date" id="" class="form-control" value="<?php echo $valuePastEx1; ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <?php if ($_SERVER['REQUEST_METHOD'] === 'GET') :
        if ((isset($_GET['currentDate']) && ($_GET['currentDate'])) && (isset($_GET['pastDate']) && ($_GET['pastDate']))) :
            $valueCurrEx1 = $_GET['currentDate'];
            $valuePastEx1 = $_GET['pastDate'];
            $resultEx1 = countDayStrtotime($valuePastEx1, $valueCurrEx1)
    ?>
            <span>Number day(s) from <?php echo $valuePastEx1 ?> to <?php echo $valueCurrEx1 ?> is: </span>
            <span class="ex-result"><?php echo $resultEx1 ?> day(s)</span>
    <?php endif;
    endif; ?>
    <hr>

    <!-- Exercise 2 -->
    <p class="ex-heading">Exercise 2</p>

    <div class="form__wrap">
        <form method="GET" action="">
            <div class="row g-3 align-items-center input-form">
                <div class="col-auto">
                    <label for="fromDate" class="col-form-label">Date</label>
                </div>
                <div class="col-auto">
                    <input placeholder="Current date" name="fromDate" type="date" id="" class="form-control" value="<?php echo $fromDate;  ?>">
                </div>
                <div class="col-auto">
                    <label for="numberDay" class="col-form-label">Number day(s)</label>
                </div>
                <div class="col-auto">
                    <input placeholder="Number days" name="numberDay" type="number" id="" class="form-control" value="<?php echo $numberDay;  ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <?php if ($_SERVER['REQUEST_METHOD'] === 'GET') :
        if ((isset($_GET['fromDate']) && is_string($_GET['fromDate'])) && (isset($_GET['numberDay']) && is_numeric($_GET['numberDay']))) :
            $fromDate = $_GET['fromDate'];
            $numberDay = $_GET['numberDay'];
    ?>

            <span>After <?php echo $numberDay ?> day from <?php echo $fromDate ?> is: </span>
            <span class="ex-result"><?php echo  addDay($fromDate, $numberDay) ?></span>
            <br>
            <span>Before <?php echo $numberDay ?> day from <?php echo $fromDate ?> is: </span>
            <span class="ex-result"><?php echo  minusDay($fromDate, $numberDay) ?></span>
            <br>

    <?php endif;
    endif; ?>
    <hr>

    <!-- Exercise 3 -->
    <p class="ex-heading">Exercise 3</p>
    <?php countDaysOfMonth() ?>
    <hr>

    <!-- Exercise 4 -->
    <p class="ex-heading">Exercise 4</p>
    <?php jsonToArray() ?>
    <hr>

    <!-- Exercise 5 -->
    <p class="ex-heading">Exercise 5</p>
    <?php sortArray(["a" => 31, "b" => 41, "c" => 39, "d" => 40]) ?>
    <hr>

    <!-- Exercise 6 -->
    <p class="ex-heading">Exercise 6</p>
    <?php temperature(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73); ?>
    <hr>

    <!-- Exercise 7 -->
    <p class="ex-heading">Exercise 7</p>
    <?php $arrEx7 = array(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73); ?>
    <?php countValueInArr($arrEx7, 73); ?>
    <hr>

    <p class="ex-heading">Exercise 8</p>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Status</th>
                <th scope="col">Time</th>
                <th scope="col">Type</th>
            </tr>
        </thead>
        <tbody>
            <?php readFileCSV(); ?>

        </tbody>
    </table>
    <hr>
</body>

</html>