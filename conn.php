<?php
define('SERVER_NAME', 'localhost');
define('PORT_NUMBER', 3306);
define('DB_NAME', 'products_crud');
define('USER_NAME', 'root');
define('PASSWORD', '');

try {
    $conn = new PDO("mysql:host=" . SERVER_NAME . ";port=" . PORT_NUMBER . ";dbname=" . DB_NAME, USER_NAME, PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed" . $e->getMessage();
}
